import axios from "axios";


export const fetchUsers = () => {
  return async (dispatch) => {
    const response = await axios.get(
      `http://jsonplaceholder.typicode.com/users`
    );
    dispatch({ type: "FETCH_USERS", payload: response.data });
  };
};
