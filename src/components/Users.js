import React, { useEffect } from "react";
import { fetchUsers } from "../actions";
import { useDispatch, useSelector } from "react-redux";
import Avatar from "avataaars";
import { generateRandomAvatarOptions } from "./Avatar";

function Users() {

const dispatch = useDispatch();

useEffect(() => {
  dispatch(fetchUsers());
}, []);

const listOfUsers = useSelector((state) => state.users);
console.log(listOfUsers);


  return (
    // <div className="container px-4">
    //   <div className="row gx-5">
    //     {listOfUsers.map((user) => {
    //       return (
    //         <div key={user.id} className="col-lg-3 col-sm-6 item">
    //           <Avatar
    //             style={{ width: "100px", height: "100px" }}
    //             avatarStyle="Circle"
    //             {...generateRandomAvatarOptions()}
    //           />
    //           <p>{user.name}</p>
    //           <p>{user.username}</p>
    //           <p>{user.email}</p>
    //         </div>
    //       );
    //     })}
    //   </div>
    // </div>
    <div class="container px-4">
      <div class="row gx-3 mt-3">

     {listOfUsers.map((user) => {
           return (
             <div key={user.id} className="col item">
               <Avatar
                 style={{ width: "100px", height: "100px" }}
                avatarStyle="Circle"
                 {...generateRandomAvatarOptions()}
             />
              <p>{user.name}</p>
             <p>{user.username}</p>
              <p>{user.email}</p>
            </div>
          );
         })}
       
      </div>
    </div>
  );
}

export default Users
